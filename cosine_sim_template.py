#! /usr/bin/python
# -*- coding: utf-8 -*-


"""Rank sentences based on cosine similarity and a query."""


from argparse import ArgumentParser
import numpy as np


def get_sentences(file_path):
    """Return a list of sentences from a file."""
    with open(file_path, encoding='utf-8') as hfile:
        return hfile.read().splitlines()


def get_top_k_words(sentences, k):
    """Return the k most frequent words as a list."""
    # TODO
    
    used_words = []
    #punctuation = [",", ".", "!", "?", ";", "'", "\""] 
    counter = []
    for sentence in sentences:
        for word in sentence.split():
            if word in used_words:
                index = used_words.index(word)
                counter[index] += 1
            else:
                used_words.append(word)
                counter.append(1)
    print("finished loop")

    top_words = []
    max_count = 0
    for i in range(k):
        for j in range(len(counter)):
            if counter[j] > max_count:
                max_count = counter[j]
                index = j
        top_words.append(used_words[index])
        counter[index] = 0
        max_count = 0
    return top_words

def encode(sentence, vocabulary):
    """Return a vector encoding the sentence."""
    result = np.zeros(len(vocabulary), dtype="int32")
    
    for word in sentence.split():
        if word in vocabulary:
            i = vocabulary.index(word)
            result[i] += 1
    
    return result


def get_top_l_sentences(sentences, query, vocabulary, l):
    """
    For every sentence in "sentences", calculate the similarity to the query.
    Sort the sentences by their similarities to the query.

    Return the top-l most similar sentences as a list of tuples of the form
    (similarity, sentence).
    """
    # TODO
    in_vector = encode(query, vocabulary)
    output = []
    similarities = []
    
    for sentence in sentences:
        similarities.append([cosine_sim(in_vector, encode(sentence, vocabulary)), sentence])
    
    for i in range(l):
        print(i)
        best_sim = -float('Inf')
        best_sentence = ""
        index = 0
        best_index = 0
        for sim, sent in similarities:
            if sim > best_sim:
                best_sim = sim
                best_sentence = sent
                best_index = index
            index += 1
        similarities.pop(best_index)
        output.append([best_sim, best_sentence])
            
                
    return output


def cosine_sim(u, v):
    """Return the cosine similarity of u and v."""
    # TODO

    if u.shape != v.shape:
        return 0
    cosine_sim = 0.0
    for i in range(u.shape[0]):
        cosine_sim += u[i]*v[i]
    absolute_u = 0.0
    absolute_v = 0.0
    for i in range(u.shape[0]):
        absolute_u += u[i]*u[i]
        absolute_v += v[i]*v[i]
    absolute_u = np.sqrt(absolute_u)
    absolute_v = np.sqrt(absolute_v)
    
    cosine_sim = cosine_sim / (absolute_u*absolute_v)
    return cosine_sim


def main():
    arg_parser = ArgumentParser()
    arg_parser.add_argument('INPUT_FILE', help='An input file containing sentences, one per line')
    arg_parser.add_argument('QUERY', help='The query sentence')
    arg_parser.add_argument('-k', type=int, default=1000,
                            help='How many of the most frequent words to consider')
    arg_parser.add_argument('-l', type=int, default=10, help='How many sentences to return')
    args = arg_parser.parse_args()

    sentences = get_sentences(args.INPUT_FILE)
    top_k_words = get_top_k_words(sentences, args.k)
    query = args.QUERY.lower()

    print('using vocabulary: {}\n'.format(top_k_words))
    print('using query: {}\n'.format(query))
    
    print()
    
    # suppress numpy's "divide by 0" warning.
    # this is fine since we consider a zero-vector to be dissimilar to other vectors
    with np.errstate(invalid='ignore'):
        result = get_top_l_sentences(sentences, query, top_k_words, args.l)

    print('result:')
    for sim, sentence in result:
        print('{:.5f}\t{}'.format(sim, sentence))


if __name__ == '__main__':
    main()
